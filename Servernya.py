import socket
import sys
import socket
import bz2

#RSA merupakan Asimetris
from Crypto.PublicKey import RSA
#PKCS1_OAEP merupakan Simetris
from Crypto.Cipher import PKCS1_OAEP

PORT = 8000
HOST = ''
BUFFER = 1024

# Server siap connect dengan client
sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
sock.bind((HOST, PORT))
sock.listen(5)

print(">>>> Server menyala <<<<")
print(">>>> Menunggu Client terhubung <<<<")

try:
    # MENERIMA PESAN KOSONG DARI CLIENT

    server, address = sock.accept()
    data = server.recv(BUFFER).decode()

    # SERVER GENERATE PUBLIC KEY DAN PRIVATE KEY

    rsa_key = RSA.generate(4096, e=65537)
    private_key = rsa_key.exportKey("PEM")
    public_key = rsa_key.publickey().exportKey("PEM")

    print("-- Public Key: ", public_key)
    server.send(public_key)

    # MENERIMA SHARED KEY DARI CLIENT

    data = server.recv(BUFFER)

    cipher = PKCS1_OAEP.new(rsa_key)
    shared_key = cipher.decrypt(data)

    print(">>>> Shared Key Berhasil di terima <<<<")
    print(">>>> Shared Key: ", shared_key)

    # MENERIMA FILE DARI CLIENT

    f = open("result.mp4", "wb")
    print(">>>> Sedang Menerima File <<<<")

    ByteOfFile = bytearray()

    while True:
        ValueOfFile = server.recv ( 1024 )

        while ValueOfFile:
            ByteOfFile += ValueOfFile
            ValueOfFile = server.recv(1024)


        decompressed = bz2.decompress(ByteOfFile)
        f.write(decompressed)
        f.close()
        print(">>>> Berhasil menerima file dari Client<<<< ")

except Exception as e:
    sock.close()
    print(e)
