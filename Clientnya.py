import socket
import sys
import os
import base64
import pickle
import bz2

#RSA merupakan Asimetris
from Crypto.PublicKey import RSA
#PKCS1_OAEP merupakan Simetris
from Crypto.Cipher import PKCS1_OAEP
from Crypto.Random import get_random_bytes



HOST = "localhost"
PORT = 8000
BUFFER = 1024

#CLIENT MENGHUBUNGKAN DENGAN SERVER
sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
sock.connect((HOST, PORT))

print(">>>> Client Menyala <<<<")

try:
    sock.send(" ".encode())
    server_public_key = sock.recv(BUFFER).decode()

    # GENERATE RSA SHARED KEY

    print(">>>> Generate RSA (Shared Key) <<<<")

    key = RSA.importKey(server_public_key)

    shared_key_length = 16
    shared_key = os.urandom(shared_key_length)
    encoded_shared_key = base64.b64encode(shared_key)

    # ENKRIPSI SHARED KEY DENGAN PUBLIC KEY DARI SERVER

    cipher = PKCS1_OAEP.new(key)
    sent = sock.send(cipher.encrypt(encoded_shared_key))
    print(">>>> Berhasil Enkripsi Shared Key dengan Public Key dari Server <<<<")


    #MENGIRIM FILE MENUJU SERVER

    print(">>>> Proses mengirim file menuju server <<<<")
    f = open("Video.mp4", "rb")
    ValueOfFile = f.read()
    ByteofFile = bytearray()

    print(">>>> Proses Compressing dengan BZ2 <<<<")
    comp = bz2.compress(ValueOfFile)
    #compress = PKCS1_OAEP(encoded_shared_key).encrypt(comp)
    ByteofFile += comp

    j=0
    part = comp[j * 1024:(j + 1) * 1024]

    while(part):
        print(">>>> Sending Part... <<<<<")
        sock.send(part)
        j += 1
        part = comp[j * 1024: (j + 1) * 1024]
        print(">>>> Successfully sending file... <<<<")

    f.close()

except Exception as e:
    print(e)
